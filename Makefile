.PHONY: install-argo
install-argo:
	helm -n argo-cd install argo-cd argo/argo-cd --version 4.10.4 -f ./argocd/values.yaml --create-namespace 